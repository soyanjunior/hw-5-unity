﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;

namespace HW_5._1
{
    static class MatrixInfo
    {
        /// <summary>
        ///  получаем количество строк матрицы
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static int RowsCount(this int[,] matrix)
        {
            return matrix.GetUpperBound(0) + 1;
        }

        /// <summary>
        /// получаем количество столбцов матрицы
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static int ColumnsCount(this int[,] matrix)
        {
            return matrix.GetUpperBound(1) + 1;
        }
    }
    class Program
    {
        #region Общие методы
        /// <summary>
        /// метод получения матрицы из консоли
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static int[,] GetMatrixFromConsole(string name)
        {
            Random rand = new Random();
            Console.Write("Количество строк матрицы {0}:    ", name);
            var n = int.Parse(Console.ReadLine());
            Console.Write("Количество столбцов матрицы {0}: ", name);
            var m = int.Parse(Console.ReadLine());

            var matrix = new int[n, m];
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < m; j++)
                {
                    matrix[i, j] = rand.Next(20);
                }
            }
            return matrix;
        }
        /// <summary>
        /// метод для вывода матриц на экран
        /// </summary>
        /// <param name="matrix"></param>
        static void PrintMatrix(int[,] matrix)
        {
            for (var i = 0; i < matrix.RowsCount(); i++)
            {
                for (var j = 0; j < matrix.ColumnsCount(); j++)
                {
                    Console.Write(matrix[i, j].ToString().PadLeft(4));
                }

                Console.WriteLine();
            }
        }
        #endregion

        #region  1.1. Создать метод, принимающий число и матрицу, возвращающий матрицу умноженную на число
        static void MatrixNumber(int[,] Matrix, double number)
        {
            double[,] Mat = new double[Matrix.GetLength(0), Matrix.GetLength(1)];
            for (int i = 0; i < Matrix.GetLength(0); i++)
            {
                for (int j = 0; j < Matrix.GetLength(1); j++)
                {
                    Mat[i, j] = Matrix[i, j] * number;
                    Console.Write(Mat[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
        #endregion

        #region 1.2. Создать метод, принимающий две матрицу, возвращающий их сумму
        /// <summary>
        /// метод для слажения двух матриц
        /// </summary>
        /// <param name="matrixA"></param>
        /// <param name="matrixB"></param>
        /// <returns></returns>
        static int[,] AmountMatrix(int[,] matrixA, int[,] matrixB)
        {
            var matrixC = new int[matrixA.RowsCount(), matrixB.ColumnsCount()];
            for(int i=0; i<matrixA.RowsCount(); i++)
            {
                for (int j = 0; j<matrixB.ColumnsCount(); j++)
                {
                    matrixC[i, j] = matrixA[i, j] + matrixB[i, j];
                }
            }
            return matrixC;
        }
        #endregion

        #region  1.3. Создать метод, принимающий две матрицу, возвращающий их произведение

        /// <summary>
        /// метод умножения матрицы на матрицу
        /// </summary>
        /// <param name="matrixA"></param>
        /// <param name="matrixB"></param>
        /// <returns></returns>
        static int[,] MatrixMultiplication(int[,] matrixA, int[,] matrixB)
        {
            if (matrixA.ColumnsCount() != matrixB.RowsCount())
            {
                throw new Exception("Умножение не возможно! Количество столбцов первой матрицы не равно количеству строк второй матрицы.");
            }

            var matrixC = new int[matrixA.RowsCount(), matrixB.ColumnsCount()];

            for (var i = 0; i < matrixA.RowsCount(); i++)
            {
                for (var j = 0; j < matrixB.ColumnsCount(); j++)
                {
                    matrixC[i, j] = 0;

                    for (var k = 0; k < matrixA.ColumnsCount(); k++)
                    {
                        matrixC[i, j] += matrixA[i, k] * matrixB[k, j];
                    }
                }
            }
            return matrixC;
        }
        #endregion

        static void Main(string[] args)
        {
            #region выполнение задания 1.1
            Console.WriteLine("Программа умножения матрицы на число");
            int[,] matr = { { 2, 3, 4 }, { 5, 2, 1 } };
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    Console.Write(matr[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("После умножения");
            MatrixNumber(matr, 2);
            Console.ReadKey();
            #endregion

            #region выполнение задания 1.2
            Console.WriteLine("Программа для сложения двух матриц");
            var a = GetMatrixFromConsole("A");
            var b = GetMatrixFromConsole("B");

            Console.WriteLine("Матрица A:");
            PrintMatrix(a);

            Console.WriteLine("Матрица B:");
            PrintMatrix(b);

            var result = AmountMatrix(a, b);
            Console.WriteLine("Сложение двух матриц: \n");
            PrintMatrix(result);
            #endregion

            #region выполнение задания 1.3
            Console.WriteLine("Программа для умножения матриц");

            var x = GetMatrixFromConsole("A");
            var y = GetMatrixFromConsole("B");

            Console.WriteLine("Матрица A:");
            PrintMatrix(x);

            Console.WriteLine("Матрица B:");
            PrintMatrix(y);

            var multy = MatrixMultiplication(x, y);
            Console.WriteLine("Произведение матриц:\n");
            PrintMatrix(multy);

            Console.ReadKey();
            #endregion
        }
    }
}

